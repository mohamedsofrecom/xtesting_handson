# Xtesting user guidelines

Describe how to use xtesting



## Why xtesting


### Deployment ecosystem is complex

Different virtualization levels need to be validated

![Ecosystem](./images/ComplexEcosystem.PNG)


### Automatic testing is key to

* To validate modules of any size and at any level (from openstack to the VNF modules)
* Validate End to End solutions
* Prove interroperability between systems
* Ensure sustainability
* Give performance figures and elaborate KPIs
* Ensure backward compatibilities
* Provide trustability evidence


### Richness, diversity of testing environments

* Lots of testing organizations/cultures
* Lots of test frameworks (Robot, python, bash ...)
* Lots of options to integrate in CI (jenkins, gitlab-ci)



## What is xtesting


### Simple solution

xtesting is a simple solution to facilitate the integration of these different testing environments

* simplify test integration in a complete CI/CD toolchain (e.g. Jenkins or gitlab-ci,
Testing Containers, Test API and dashboard)
* allow a proper design and verify multiple components in the same CI/CD
toolchain (OpenStack, Kubernetes, CNF/VNF, etc.)


### xtesting facilitates the tester life

* ease the development of third-party test cases by offering multiple
drivers: Python, Bash, unittest, robot framework, behave framework
* manage requirements and offer lightweight Docker images
* unify the test triggering based a central file: testcases.yaml
* unify the presentation of test results


### Testcases definition (1/2)

* Example of testcases.yaml with bash test

```yaml
---
tiers:
    -
        name: bashtest
        order: 1
        description: ''
        testcases:
            -
                case_name: bashecho
                project_name: hellobash
                criteria: 100
                blocking: true
                clean_flag: false
                description: ''
                run:
                    name: 'bashfeature'
                    args:
                        cmd: 'echo -n Bash test; exit 0'
```


### Testcases definition (2/2)

* Specific variables
  * criteria: A value *100* means all tests shall be successfull to have the
  status "PASSED"
  * blocking: Stop the test execution if the testcase fails
  * clean_flag: Clean resources after test execution (Resources action to be
  defined in python function)


### Unify test results

* [Log example](./examples/bash_test.log)
* [ONAP integration example](http://testresults.opnfv.org/onap-integration/)


### xtesting architecture

![Architecture](./images/xtesting_archi.PNG)


### xtesting environnement variables (1/3)

* Test context information that will be set in database depending on env
variable
  * INSTALLER_TYPE: demo
  * DEPLOY_SCENARIO: demo-test
  * NODE_NAME: test_pod
  * BUILD_TAG: Release_X


### xtesting environnement variables (2/3)

* MongoDB database

  * TEST_DB_URL: URL of the test DB API (Test API linked to mongoDB)
  * TEST_DB_EXT_URL: URL of the external DB API


### xtesting environnement variables (3/3)

* S3 Storage (Minio): all objects under /var/lib/xtesting/results are pushed

  * S3_ENDPOINT_URL (http://127.0.0.1:9000)
  * S3_DST_URL (s3://xtesting/prefix)
  * HTTP_DST_URL (http://127.0.0.1/prefix)
  * Credentials: AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY


### Use ansible galaxy role to deploy adhoc xtesting platform (1/3)

* This ansible role allows to deploy by default:
  * Jenkins: 8080
  * Minio: 9000
  * S3www: 8181
  * TestAPI: 8000
  * Docker Registry: 5000


### Use ansible galaxy role to deploy adhoc xtesting platform (2/3)

* And the following modules can be also deployed
  * GitLab: 80
  * InfluxDB: 8086
  * Grafana: 3000
  * And also in last versions: cachet, postgres


### Use ansible galaxy role to deploy adhoc xtesting platform (3/3)

* [Variables list](https://github.com/collivier/ansible-role-xtesting/blob/master/defaults/main.yml)
* Command to run: ansible_playbook [site.yml](./examples/site.yml)


### Create its own test container (1/3)

* Create its own docker file including tests to launch
([Dockerfile](./examples/Dockerfile) example)
  * The testcases are defined in [testcases.yaml](./examples/testcases.yaml)


### Create its own test container (2/3)

* The testcases are executed with the following options
  * `-t`: Test case or tier (group of tests) to be executed. It will run all the
  test if not specified.
  * `-r`: Allow the report sending towards configured MongoDB (default=false)
  * `-p`: Push artifacts under /var/lib/xtesting/results towards S3 storage
  module (Minio) (default=false)
  * `-n`: Do not clean OpenStack resources after running each test
  (default=false)


### Create its own test container (3/3)

* Command to launch in the same directory as Dockerfile  
  *sudo docker build -t 127.0.0.1:5000/hello .*
* Store this docker file in a registry  
  *sudo docker push 127.0.0.1:5000/hello*
* Run the created docker image from a CI scheduler (Jenkins or Gitlab-CI)
* The env variables can be set via a specific [env file](./examples/env.list) while running the
container  
 *sudo docker run -t --env-file ./env.list 127.0.0.1:5000/hello*



## Reference

* [Readthedoc xtesting documentation](https://xtesting.readthedocs.io/en/latest/)
* [How to deploy your own CI/CD toolchains](https://wiki.opnfv.org/pages/viewpage.action?pageId=32015004)
